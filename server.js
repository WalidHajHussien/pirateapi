const express = require('express');
const bodyParser = require('body-parser');
var Pirate = require('./db');
var jwt = require('jsonwebtoken');
const request = require('request')


// Note: add express server 
const app =  express();

//Note: add bodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.get('/pirates', function (request, response) {

	Pirate.find({}).then(function(data){
		var token=jwt.sign({ userName: 'pirate' }, 'ozil');
		 response.send({data:data,token:token});
	})

  
});

app.get('/pirates/countPirates',checkToken, function (req, res) {

var token=req.token;
  request('https://eila-pirate-api.herokuapp.com/pirates/prison', function (err, response, body) {
    var obj=JSON.parse(body);
    var faces=obj.faces
    var count=findFace(faces);
    res.send({piratesFound:count});

  })



  
});



const PORT = process.env.PORT || 5000;
app.listen(PORT, function () {
   console.error(`Server listening on port ${PORT}`);
});

// Authorization : Bearer token 
function checkToken(req,res,next){
	const autho=req.headers['authorization'];
	
	if(autho){
		var bearer=autho.split(' ');
		var token=bearer[2];
		req.token=token;
		next();

	}else{
		res.send('token not correct, please add the token to the req Header ex : Authorization :Bearer  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6InBpcmF0ZSIsImlhdCI6MTU0ODMyOTk3OH0.KSK-83REKNKpeUrbJ1qgNyPzrggNpSt8ZPdB5m7bmfw,you can get the token by call the api "https://piratesapi.herokuapp.com/pirates" ')
	}


}

// count pirates :
function findFace(array){
    var eye=[';','8']
    var nose=['-','~']
    var mouth=[')','|']
    var count=0;

    for (var i=0;i<array.length;i++){
        var face=array[i];
        if(face.length===2){
            if((face.indexOf('8')!=-1 || face.indexOf(';')!=-1) && (face.indexOf(')')!=-1 || face.indexOf('|')!=-1)){
                count=count+1;
            }

        }else{
            if((face.indexOf('8')!=-1 || face.indexOf(';')!=-1) && (face.indexOf(')')!=-1 || face.indexOf('|')!=-1) && (face.indexOf('-')!=-1 || face.indexOf('~')!=-1) ){
                count=count+1;
            }

        }


    }

    return count;


}
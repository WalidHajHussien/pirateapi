const mongoose = require('mongoose');



// create the connection 
mongoose.connect('mongodb://ozil:ozil123@ds131814.mlab.com:31814/ozil')

var db = mongoose.connection;

db.on('error', function () {
  console.log('mongoose connection error');
});

db.once('open', function () {
  console.log('mongoose connected successfully');
});

// create the schema
const Schema = mongoose.Schema;

const PiratesSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Name Field is Required']
    },

    age:{
        type:Number,
        required: [true, 'Name Field is Required']
    },
    isCaptured: {
        type: Boolean,
        required: [true, 'Name Field is Required']
    }
});

var Pirate = mongoose.model('Pirate', PiratesSchema);

// to save all the data 
function addPirate(array){
	for (var i=0; i<array.length ; i++){
		Pirate.create(array[i])
	}
}

// to get the all the data 
// Pirate.find({}).then(function(Pirates){
// 	console.log(Pirates);
// })

// to save the single data 
// Pirate.create({
//     "name": "Jack sparrow",
//     "age":32,
// "isCaptured": true
// }).then(function(res){
// 	console.log(res)
// })



// data which need to save 
// var array=[
// {
//     "name": "Jack sparrow",
//     "age":32,
// "isCaptured": true
// },
// {
//    "name": "Black beard",
//    "age":45,
// "isCaptured": false
// },

// {
//    "name": "William Kidd",
//    "age":56,
// "isCaptured": true
// },
// {
//    "name": "Anne Bonny",
//    "age":28,
// "isCaptured": false
// },
// {
//    "name": "Ching Shih",
//    "age":69,
// "isCaptured": false
// }
// ]

// addPirate(array);


module.exports = Pirate;